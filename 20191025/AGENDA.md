# SOLARIS MAX IV follow-up meeting on October 25th 2019

## Questions

* SOLARIS MAX IV project sharing - where? how? when?
* Pausing Sardana scans during beam refill - MAX IV solutions
* High-data-rate detectors in use at Solaris... Eiger?  Pilatus?  Other?
* How is the detector data saved for user take-home?
* Machine or beamline simulators in use at Solaris?
* MAX IV diagnostic beamline ([this](https://www.maxiv.lu.se/science/accelerator-physics/current-projects/max-iv-diagnostic-beamlines/))- PLC, software, usage
