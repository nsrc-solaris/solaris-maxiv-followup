# SOLARIS MAX IV follow-up meeting on August 06th 2019

## Questions

* Solaris's no lima Tango Device for Basler Cameras
* Solaris's Synoptic
* Does Solaris deploy software to machine or beamline via docker container?  If so, does it use GitLab CI to do it?
* Tango on Kubernetes (k8s) - experiences from MAXIV
* Wiki (solution used, configuration, using with directory services) - carried over from previous meeting
* Solaris' experience with ISO27001
* Visit at MAX IV and its agenda
