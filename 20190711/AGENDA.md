# SOLARIS MAX IV follow-up meeting on July 11th 2019

## Questions

* MAXIV's Ansible testing with Docker
* David Erb will give a 10 minute screen-share of using Docker to test Ansible and answer questions
* Solaris' way of using Sphinx to create Python documentation
* MAXIV's Ansible Tower setup
* MAXIV's packaging (especially external dependecies i. e. Python)
* Wiki (solution used, configuration, using with directory services)
